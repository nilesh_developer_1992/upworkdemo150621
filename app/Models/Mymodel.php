<?php 
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class Mymodel extends Model
{
    protected $table = 'register';
    protected $allowedFields = ['first_name','last_name','birth_date','email', 'password','gender','address'];
}
?>