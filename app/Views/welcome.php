<!DOCTYPE html>
<html>
<head>
	<title>codeigniter Crud</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>/select2/dist/css/select2.min.css">
	<script src="<?php echo base_url();?>/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('js/dataTables.bootstrap5.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/jquery.dataTables.min.js');?>"></script>
</head>
<body>
	<div class="container">
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<form action="" id="frm_user" method="POST">
					  <div class="form-group">
					    <label >First Name</label>
					    <input type="text" class="form-control" id="first_name" name="first_name" value="">
					    
				        <span id="error_firstname" style="color: red;"></span>
					  </div>
					  <div class="form-group">
					    <label >Last Name</label>
					    <input type="text" class="form-control" id="last_name" name="last_name" value=""><br>
					      <span id="error_lastname" style="color: red;"></span>
					
					  </div>
					   <div class="form-group">
					    <label>Date of Birth</label>
					    <input type="date" class="form-control" id="dob" name="dob" value=""><br>
					     <span id="error_birthdate" style="color: red;"></span>
					  
					  </div>
					  <div class="form-group">
					    <label>Email</label>
					    <input type="email" class="form-control" id="email" name="email" value=""><br>
					     <span id="error_email" style="color: red;"></span>
					  
					  </div>
					  <div class="form-group">
					    <label>Password</label>
					    <input type="password" class="form-control" id="password" name="password" value=""><br>
					     <span id="error_password" style="color: red;"></span>
					  
					  </div>
					  <div class="form-group">
					    <label>Gender</label>
					       <select class="form-control select2 tab" style="width: 100%;" name="gender" id="gender">
						       	<option value="">Select</option>
						       	<option value="male"<?php if(isset($id)) { if($user['gender'] == "male"){echo "selected";}}?>>Male</option>
						       	<option value="female"<?php if(isset($id)) { if($user['gender'] == "female"){echo "selected";}}?>>Female</option>
					       </select><br>
					        <span id="error_gender" style="color: red;"></span>
					       
					  </div>
					  <div class="form-group">
					    <label>Address</label>
					    <input type="text" class="form-control" id="address" name="address" value="<?php if(isset($id)) { echo $user['address'];}?>"><br>
					     <span id="error_address" style="color: red;"></span>
					 
					  </div>
					  <input type="button" class="btn btn-primary" id="btnsubmit" value="Submit">
					</form>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
	
</body>
</html>
<script type="text/javascript">
	$('.select2').select2()
       
        $('#example1').DataTable({
          "pageLength": 100,
          "lengthMenu": [10, 15, 25, 50, 100, 500, 1000]
  });	

   $(document).on('click', '#btnsubmit', function() {

   		var isError = true;
	    var first_name = $("#first_name").val();
	    var last_name = $("#last_name").val();
	    var dob = $("#dob").val();
	    var email = $("#email").val();
	    var password = $("#password").val();
	    var gender = $("#gender").val();
	    var address = $("#address").val();

	    

    	if (first_name == "") {
    		 $("#error_firstname").html("First Name is required");
    		 isError = true;
    	}else {

    		 var text =/^[A-Za-z]+$/;
    		 var validtext = text.test($("#first_name").val());

    		 if (!validtext) {
    		 	 $("#error_firstname").html("First Name is required with allow Only Alpha");
    		 	 isError = true;
    		 }else {
    		 	 $("#error_firstname").html(""); 
    		 	 isError = false;
    		 }
    	}

    	if (last_name == "") {
    		 $("#error_lastname").html("Last Name is required");
    		 isError = true;
    	}else {

    		 var text =/^[A-Za-z]+$/;
    	     var validlastname = text.test($("#last_name").val());

    		 if (!validlastname) {
    		 	 $("#error_lastname").html("Last Name is required with allow Only Alpha");
    		 	 isError = true;
    		 }else {
    		 	 $("#error_lastname").html(""); 
    		 	 isError = false;
    		 }
    	}


    	if (dob == "") {
    		 $("#error_birthdate").html("The Birth Date is required");
    		 isError = true;
    	}else {

    		 $("#error_birthdate").html(""); 
    		 isError = false;
    	}

    	if (email == "") {
    		 $("#error_email").html("Email is required");
    		 isError = true;
    	}else {

    		  var ex=/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    	      var validemail = ex.test($("#email").val());

    		 if (!validemail) {
    		 	 $("#error_email").html("Email Required with Valid Email formate");
    		 	 isError = true;
    		 }else {
    		 	 $("#error_email").html(""); 
    		 	 isError = false;
    		 }
    	}


    	if (password == "") {
    		 $("#error_password").html("Password is required");
    		 isError = true;
    	}else {

    		  var pass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/
    		  var validpass = pass.test($('#password').val());
    		 
    		 if (!validpass) {
    		 	 $("#error_password").html("Password must contain Alphabal, Number and Special Character.");
    		 	 
    		 	 isError = true;
    		 }else {
    		 	 $("#error_password").html(""); 
    		 	 isError = false;
    		 }
    	}

    	if (gender == "") {
    		 $("#error_gender").html("Please Select Gender");
    		 isError = true;
    	}else {

    		 $("#error_gender").html(""); 
    		 isError = false;
    	}


		if (address == "") {
    		 $("#error_address").html("Address field is required");
    		 isError = true;
    	}else {

    		 $("#error_address").html(""); 
    		 isError = false;
    	}



   


  /*  if(first_name == "" || last_name == "" || dob == "" || email == "" || password == "" || gender == "" ||address == "")
    {
      if(first_name == '' || !validtext){
        $("#error_firstname").html("First Name is required with allow Only Alpha");
      }else{ $("#error_firstname").html(""); }
      if(last_name == '' || !validlastname){
        $("#error_lastname").html("Last Name is required with allow Only Alpha");
      }else{ $("#error_lastname").html(""); }
      if(dob == ''){
        $("#error_birthdate").html("The Birth Date is required.");
      }else{ $("#error_birthdate").html(""); }
      if(email == ''  || !validemail){
         $("#error_email").html("Email Required with Valid Email formate");
      }else{
         $("#error_email").html("");
      }
      if(password == '' || !validpass){
      	alert("dddd");
         $("#error_password").html("Password field is required");
      }else{
         $("#error_password").html("");
      }
      if(gender == ''){
         $("#error_gender").html("Please Select Gender");
      }else{
         $("#error_gender").html("");
      }
      if(address == ''){
         $("#error_address").html("Address field is required");
      }else{
         $("#error_address").html("");
      }
    }
    else
    {
    	var data = $("#frm_user").serialize();

          $.post({
        //  url: '<?php echo base_url(); ?>/user/insert',
          data:data,

          success: function (result) {
          	
            //window.location.replace("<?php echo base_url(); ?>/");
          }
        });
    }


    */


    if (!isError) {

    	var data = $("#frm_user").serialize();

          $.post({
          url: '<?php echo base_url(); ?>/user/insert',
          data:data,

          success: function (result) {
          	
            window.location.replace("<?php echo base_url(); ?>/");
          }
        });

    }
  });

</script>
 