<!DOCTYPE html>
<html>
<head>
	<title>User List</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('js/dataTables.bootstrap5.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/jquery.dataTables.min.js');?>"></script>
</head>
<body>
	<div class="container">
	  
	  	<a class="btn btn-primary"  href="<?php echo site_url('Home/create') ?>">Add</a>
		<br><br>

		<table id="example" class="table table-striped" style="width:100%">
		 <thead>
            <tr>
                <th>First Name</th>
                <th>Last name</th>
                <th>Date of Birth</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
        </thead>
           <tbody>
                 <?php
                    if (!empty($user)) {
                      foreach ($user as $key => $value) { ?>
                        <tr>
                            <td> <?php echo $value['first_name']; ?> </td>
                            <td> <?php echo $value['last_name']; ?> </td>
                            <td><?php echo date("d-m-Y", strtotime($value['birth_date']));?> </td>
                            <td> <?php echo $value['email']; ?> </td>
                            <td> <?php echo $value['gender']; ?> </td>
                            <td> <?php echo $value['address']; ?> </td>
                       		<td>
                       			<a  href="<?php echo base_url(); ?>/Home/edit/<?php echo $value['id']; ?>">Edit</a>&nbsp;&nbsp;&nbsp;
                       			<a  href="<?php echo base_url(); ?>/Home/duplicate/<?php echo $value['id']; ?>">Copy</a>&nbsp;&nbsp;&nbsp;
                       			<a  href="<?php echo base_url(); ?>/Home/delete/<?php echo $value['id']; ?>">Delete</a>
                       		</td>
                           
                          </tr>
                        <?php
                      }
                    }
                 ?>
                </tbody>  
	</table>	
	</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>