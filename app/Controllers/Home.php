<?php

namespace App\Controllers;
use App\Models\Mymodel;

class Home extends BaseController
{
	public function index()
	{
		$model = new Mymodel();
 
        $data['user'] = $model->orderBy('id', 'DESC')->findAll();
		return view('userlist',$data);
	}

	public function create(){
		return view('welcome');
	}

/*	public function insert(){
		helper(['form', 'url']);

		 $input = $this->validate([
            'first_name' => 'required|min_length[3]',
            'last_name' => 'required',
            'dob' => 'required',
            'email' => 'required|valid_email',
            'password' => 'required|max_length[10]',
            'gender' => 'required',
            'address' => 'required',
        ]);


		if (!$input) {
            echo view('welcome', [
                'validation' => $this->validator
            ]);
        } else {
        	$model = new Mymodel();
	        $data = [
	            'first_name' => $this->request->getVar('first_name'),
	            'last_name'  => $this->request->getVar('last_name'),
	            'birth_date'  => $this->request->getVar('dob'),
	            'email'  => $this->request->getVar('email'),
	            'password'  => $this->request->getVar('password'),
	            'gender'  => $this->request->getVar('gender'),
	            'address'  => $this->request->getVar('address'),
	            ];

       	 	$model->insert($data);
 			return redirect()->to( base_url('Home') );
        } 

         
      
	}*/
	public function insert(){
		

		extract($_POST);
		
		$model = new Mymodel();
	        $data = [
	            'first_name' => $first_name,
	            'last_name'  => $last_name,
	            'birth_date'  => $dob,
	            'email'  => $email,
	            'password'  => $password,
	            'gender'  => $gender,
	            'address'  =>$address,
	            ];
	       
       	$res =  $model->insert($data);
 			
	}

	public function Edit($id){
		$model = new Mymodel();
 
	    $data['user'] = $model->where('id', $id)->first();
	 	$data['id'] = $id;
	     return view('edituser', $data);
	    }
 
	public function update()
	{  
	 
	 	extract($_POST);
		
		$model = new Mymodel();
		 
	        $data = [
	            'first_name' => $first_name,
	            'last_name'  => $last_name,
	            'birth_date'  => $dob,
	            'email'  => $email,
	            'password'  => $password,
	            'gender'  => $gender,
	            'address'  =>$address,
	            ];
		 $save = $model->update($id,$data);
		
	}

	public function delete($id = null){
 		$model = new Mymodel();
 		$data['user'] = $model->where('id', $id)->delete();
 		return redirect()->to( base_url('Home') );
    }

    public function duplicate($id){
    	$model = new Mymodel();

	    $data = $model->where('id', $id)->first();

	    $model = new Mymodel();
	        $data1 = [
	            'first_name' => $data['first_name'],
	            'last_name'  => $data['last_name'],
	            'birth_date'  => $data['birth_date'],
	            'email'  => $data['email'],
	            'password'  => $data['password'],
	            'gender'  => $data['gender'],
	            'address'  =>$data['address'],
	            ];
	            // print_r($data1);
        	$model->insert($data1);
        	return redirect()->to( base_url('Home') );
    }

}
